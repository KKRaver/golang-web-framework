package models

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
)

type (
	User struct {
		gorm.Model
		Username string `json:"username" gorm:"unique"`
		Password string `json:"password"`
		Email string `json:"email" gorm:"unique"`
	}
	Token struct {
		UserID uint
		Name string
		Email string
		*jwt.StandardClaims
	}
)
