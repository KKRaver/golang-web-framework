package data

import (
	"gorm.io/gorm"
	"golang.org/x/crypto/bcrypt"
	"postgres/models"
)

type UserRepository struct {
	DB *gorm.DB
}

func (r *UserRepository) Create(user *models.User) error {
	pass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.Password = string(pass)
	db := r.DB.Create(user)
	return db.Error
}

func (r *UserRepository) CompareAuth(username, password string) (*models.User, error) {
	user := &models.User{}

	if err := r.DB.Where("Username = ?", username).Find(user).Error; err != nil {
		return nil, err
	}

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return nil, err
	}
	return user, nil
}

func (r *UserRepository) FindOne(id string) (*models.User, error) {
	user := &models.User{}

	if err := r.DB.Where("id = ?", id).Find(user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

func (r *UserRepository) GetAll() ([]models.User, error) {
	var users []models.User
	if err := r.DB.Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}