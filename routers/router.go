package routers

import (
	"github.com/gorilla/mux"
	"postgres/middleware"
)

func InitRoutes() *mux.Router {
	router := mux.NewRouter().StrictSlash(false)
	router.Use(middleware.CommonMiddleware)
	router.Use(middleware.Recovery)
	router = SetUserRoutes(router)

	auth := router.PathPrefix("").Subrouter()
	auth.Use(middleware.JwtVerify)
	auth = SetAuthUserRoutes(auth)
	return router
}