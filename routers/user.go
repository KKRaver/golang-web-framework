package routers

import (
	"github.com/gorilla/mux"
	"postgres/controllers"
)

func SetUserRoutes(router *mux.Router) *mux.Router {
	router.HandleFunc("/user/register", controllers.CreateUser).Methods("POST")
	router.HandleFunc("/user/login", controllers.LoginUser).Methods("POST")
	return router
}

func SetAuthUserRoutes(router *mux.Router) *mux.Router {
	router.HandleFunc("/user/", controllers.GetUsers).Methods("GET")
	router.HandleFunc("/user/{id}", controllers.GetUser).Methods("GET")
	return router
}