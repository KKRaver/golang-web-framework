package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"postgres/common"
	"postgres/routers"
)

func main() {
	common.StartUp()
	router := routers.InitRoutes()
	server := &http.Server{
		Addr: getPort(),
		Handler: router,
	}
	log.Println("Listening...")
	err := server.ListenAndServe()
	if err != nil {
		log.Fatalf("Failed to start server")
	}
}

func getPort() string {
	var port = os.Getenv("PORT")
	if port == "" {
		port = common.AppConfig.Server
		fmt.Println("INFO: No PORT env var detected, defaulting to "+ port)
	}
	return ":" + port
}