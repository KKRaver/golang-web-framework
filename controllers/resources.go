package controllers

import "postgres/models"

type (
	UserResource struct {
		Data models.User `json:"data"`
	}
	UsersResource struct {
		Data []models.User `json:"data"`
	}
)
