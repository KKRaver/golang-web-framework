package controllers

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"postgres/common"
	"postgres/data"
	"postgres/models"
	"time"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	var dataResource models.User
	err := json.NewDecoder(r.Body).Decode(&dataResource)
	if err != nil {
		common.ReturnAppError(
			w,
			err,
			"Invalid User data",
			http.StatusBadRequest,
		)
		return
	}
	user := &dataResource
	db := common.GetSession()
	repo := &data.UserRepository{DB: db}
	err = repo.Create(user)
	if err != nil {
		common.ReturnAppError(
			w,
			err,
			"Could not create User",
			http.StatusBadRequest,
			)
		return
	}
	user.Password = ""
	j, err := json.Marshal(UserResource{Data: *user});
	if err != nil {
		common.ReturnAppError(
			w,
			err,
			"An unexpected Error occurred",
			http.StatusInternalServerError,
		)
		return
	}
	w.WriteHeader(http.StatusCreated)
	_, err = w.Write(j)
	if err != nil {
		log.Printf("[AppError]: Failed to send response, %v\n", err)
	}
}

func LoginUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		common.ReturnAppError(
			w,
			err,
			"Invalid User data",
			http.StatusBadRequest,
		)
		return
	}
	db := common.GetSession()
	repo := data.UserRepository{DB: db}
	loggedUser, err := repo.CompareAuth(user.Username, user.Password)
	if err != nil {
		common.ReturnAppError(
			w,
			err,
			"Could not authenticate User",
			http.StatusBadRequest,
			)
		return
	}
	expiresAt := time.Now().Add(time.Minute * 1000).Unix()
	tk := &models.Token{
		UserID: user.ID,
		Name: user.Username,
		Email: user.Email,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)

	tokenString, err := token.SignedString([]byte(common.AppConfig.Secret))
	if err != nil {
		common.ReturnAppError(
			w,
			err,
			"Could not generate Token",
			http.StatusInternalServerError,
			)
		return
	}
	loggedUser.Password = ""
	resp := map[string]interface{}{"token": tokenString, "user": loggedUser}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	j, err := json.Marshal(resp)
	if err != nil {
		common.ReturnAppError(
			w,
			err,
			"Cannot parse json data",
			http.StatusInternalServerError,
		)
		return
	}
	_, err = w.Write(j)
	if err != nil {
		log.Printf("[AppError]: Failed to send response, %v\n", err)
	}
}

func GetUsers(w http.ResponseWriter, r *http.Request) {
	db := common.GetSession()
	repo := &data.UserRepository{DB: db}
	users, err := repo.GetAll()
	if err != nil {
		common.ReturnAppError(
			w,
			err,
			"Could not fetch Users from Database",
			http.StatusInternalServerError,
			)
	}
	for index, user := range users {
		user = users[index]
		user.Password = ""
		users[index] = user
	}
	j, err :=json.Marshal(users)
	if err != nil {
		common.ReturnAppError(
			w,
			err,
			"Could not parse Users to JSON",
			http.StatusInternalServerError,
		)
	}
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(j)
	if err != nil {
		log.Printf("[AppError]: Failed to send response, %v\n", err)
	}
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	db := common.GetSession()
	repo := data.UserRepository{DB: db}
	user, err := repo.FindOne(id)
	if err != nil {
		common.ReturnAppError(
			w,
			err,
			"Cannot fetch user",
			http.StatusInternalServerError,
			)
	}
	user.Password = ""
	j, err := json.Marshal(user)
	if err != nil {
		common.ReturnAppError(
			w,
			err,
			"Cannot parse json data",
			http.StatusInternalServerError,
		)
	}
	_, err = w.Write(j)
	if err != nil {
		log.Printf("[AppError]: Failed to send response, %v\n", err)
	}
}