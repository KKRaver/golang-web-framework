package common

import (
	"gorm.io/gorm"
	//_ "github.com/jinzhu/gorm/dialects/postgres"
	"gorm.io/driver/sqlite"
	"log"
	"postgres/models"
)

var database *gorm.DB

func GetSession() *gorm.DB {
	//dbConnInfo := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", AppConfig.PostgresqlHost, AppConfig.PostgresqlPort, AppConfig.DBUser, AppConfig.Database, AppConfig.DBPassword)
	if database == nil {
		var err error
		//database, err = gorm.Open("postgres", dbConnInfo)
		database, err = gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{})
		if err != nil {
			log.Fatalf("[DatabaseError]: %v\n", err)
		}
	}
	return database
}

func createDbSession() {
	//dbConnInfo := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", AppConfig.PostgresqlHost, AppConfig.PostgresqlPort, AppConfig.DBUser, AppConfig.Database, AppConfig.DBPassword)
	//db, err := gorm.Open("postgres", dbConnInfo)
	database, err := gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{})

	if err != nil {
		log.Fatalf("[DatabaseError]: %s\n", err)
	}
	createSchema(database)
}

func createSchema(db *gorm.DB) {
	var err error
	err = db.AutoMigrate(&models.User{})
	if err != nil {
		log.Fatalf("[DatabaseError]: Failed to perform migratiosn on user, %v", err)
	}
}
